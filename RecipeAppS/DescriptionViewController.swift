//
//  DescriptionViewController.swift
//  RecipeAppS
//
//  Created by Cory Dunn on 10/1/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var detailsDictionary = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        self.title = detailsDictionary["title"]
        
        let tvIngredients = UITextView.init(frame: CGRect.zero)
        tvIngredients.text = detailsDictionary["ingredients"]
        tvIngredients.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tvIngredients)
        
        let tvDirections = UITextView.init(frame: CGRect.zero)
        tvDirections.text = detailsDictionary["directions"]
        tvDirections.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tvDirections)
        
        let lblIngredients = UILabel()
        lblIngredients.text = "Ingredients:"
        lblIngredients.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(lblIngredients)
        
        let lblDirections = UILabel()
        lblDirections.text = "Directions:"
        lblDirections.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(lblDirections)
        
        
        
        let views = ["tvIngredients": tvIngredients, "tvDirections": tvDirections, "lblIngredients": lblIngredients, "lblDirections": lblDirections]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraintslbl1 = NSLayoutConstraint.constraints(withVisualFormat: "V:|[lblIngredients]-500-|",options: [], metrics: nil, views: views)
        allConstraints += verticalConstraintslbl1
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[tvIngredients]-100-|",options: [], metrics: nil, views: views)
        allConstraints += verticalConstraints
        
        let verticalConstraintslbl2 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[lblDirections]-300-|",options: [], metrics: nil, views: views)
        allConstraints += verticalConstraintslbl2
        
        let verticalConstraints1 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-300-[tvDirections]-|",options: [], metrics: nil, views: views)
        allConstraints += verticalConstraints1
        
        
        let horizontalConstraintslbl1 = NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[lblIngredients]-20-|",options: [], metrics: nil, views: views)
        allConstraints += horizontalConstraintslbl1
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[tvIngredients]-20-|",options: [], metrics: nil, views: views)
        allConstraints += horizontalConstraints
        
        let horizontalConstraintslbl2 = NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[lblDirections]-20-|",options: [], metrics: nil, views: views)
        allConstraints += horizontalConstraintslbl2
        
        let horizontalConstraints1 = NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[tvDirections]-20-|",options: [], metrics: nil, views: views)
        allConstraints += horizontalConstraints1
        
        NSLayoutConstraint.activate(allConstraints)
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

