//
//  ViewController.swift
//  RecipeAppS
//
//  Created by Cory Dunn on 10/1/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var dictionaries = [[String: String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mytableview = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain);
        mytableview.translatesAutoresizingMaskIntoConstraints = false
        mytableview.delegate = self
        mytableview.dataSource = self
        mytableview.register(UITableViewCell.self, forCellReuseIdentifier: "LabelCell")
        self.view.addSubview(mytableview)
        
        let views = ["mytableview": mytableview,
                     ]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[mytableview]-|",options: [], metrics: nil, views: views)
        allConstraints += verticalConstraints
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[mytableview]|",options: [], metrics: nil, views: views)
        allConstraints += horizontalConstraints
        
        NSLayoutConstraint.activate(allConstraints)
        
        
        // Create a dictionary and add it to the array.
        let dictionary1: [String: String] = ["title": "Grilled Cheese Sandwich", "ingredients":"Bread, Butter, Cheese", "directions":"Preheat griddle to med-high heat. Spread butter evenly across one side of each slice of bread. Place one slice of bread (butter side down) on griddle. Place cheese on top of bread slice. Add another slice of bread on top (butter side up). Flip sandwich over when bottom slice is golden brown. Remove from heat once both slices of bread are golden brown. Chow down!"]
        dictionaries.append(dictionary1)
        
        // Create another dictionary.
        let dictionary2: [String: String] = ["title": "Mac & Cheese", "ingredients":"Box of Kraft Mac & Cheese, Butter, Milk", "directions":"Place a pot of water over high heat until boiling. Add macaroni to water and cook until al-dente. Drain water from pot. Add milk, butter, and powdered cheese. Stir ingredients together and enjoy!"]
        dictionaries.append(dictionary2)
        
        let dictionary3: [String: String] = ["title": "Pizza", "ingredients":"Cellphone", "directions":"Pick up cellphone. Call Dominoes. Place order. Make payment. Open door and accept pizza from delivery kid. It's pizza time!"]
        dictionaries.append(dictionary3)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictionaries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        let dictionary = dictionaries[indexPath.row]
        
        cell.textLabel?.text = dictionary["title"]
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dictionary = dictionaries[indexPath.row]
        
        let dvc = DetailsViewController()
        dvc.detailsDictionary = dictionary
        self.navigationController?.pushViewController(dvc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

